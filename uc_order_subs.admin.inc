<?php
/**
 * @file
 * Functionality for the admin page.
 */

/**
 * Menu callback for displaying the subscription orders admin page.
 */
function uc_order_subs_admin() {
  // Build a header for the table.
  $header = array(
    array('data' => '#', 'field' => 'sub_id', 'sort' => 'desc'),
    array('data' => 'Base order', 'field' => 'order_id'),
    'Items',
    array('data' => 'Total', 'field' => 'order_total'),
    array('data' => 'User', 'field' => 'uid'),
    array('data' => 'Status', 'field' => 'active'),
    array('data' => 'Last updated', 'field' => 'updated'),
  );

  // Build a query that has a pager.
  $query = pager_query("SELECT
      subs.sub_id, subs.order_id, subs.active, subs.uid, subs.created, subs.updated,
      u.name,
      o.order_total, o.product_count
    FROM {uc_order_subs} subs
      INNER JOIN {users} u
        ON subs.uid = u.uid
      INNER JOIN {uc_orders} o
        ON o.uid = u.uid
        AND o.order_id = subs.order_id
    " . tablesort_sql($header), 10);

  // Build a list of all users.
  $rows = array();
  while ($result = db_fetch_array($query)) {
    $row = array();
    $row['sub_id'] = $result['sub_id'];
    $row['order_id'] = l($result['order_id'], 'user/' . $result['uid'] . '/order/' . $result['order_id']);
    $row['product_count'] = $result['product_count'];
    $row['order_total'] = uc_price($result['order_total']);
    $row['uid'] = l($result['name'] . ' (' .  $result['uid'] . ')', 'user/' . $result['uid'] . '/order/' . $result['order_id']);
    $row['active'] = empty($result['active']) ? 'Inactive' : 'Active';
    $row['updated'] = empty($result['updated']) ? format_date($result['created']) : format_date($result['updated']);

    $rows[] = $row;
  }

  // Build the output in a table format.
  return theme('table', $header, $rows) . theme('pager');
}
