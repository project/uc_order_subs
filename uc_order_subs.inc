<?php
/**
 * @file
 * Miscellaneous functions.
 */

/**
 * Add the discount line item to a given order.
 */
function uc_order_subs_add_discount($order_id) {
  // Remove the discount line item if it had already been added, this will
  // allow for the order to be changed prior to checkout and to automatically
  // update the total discount accordingly.
  uc_order_subs_remove_discount($order_id);

  // Add the discount line item.
  $discount = uc_order_subs_calculate_discount($order_id);
  $discount_label = uc_order_subs_discount_label();
  uc_order_line_item_add($order_id, 'generic', $discount_label, $discount);
}

/**
 * Remove the discount line item from the order.
 */
function uc_order_subs_remove_discount($order_id) {
  $order = uc_order_load($order_id);
  $discount_label = uc_order_subs_discount_label();

  // This will remove all line items matching the label.
  foreach ($order->line_items as $line_item) {
    if ($line_item['title'] == $discount_label) {
      uc_order_delete_line_item($line_item['line_item_id']);
    }
  }
}

/**
 * Generate the label that is shown on the order line item.
 */
function uc_order_subs_discount_label() {
  static $label;
  if (empty($label)) {
    $percentage = intval(variable_get('uc_order_subs_discount', 0));
    $label = "Subscription discount ({$percentage}%)";
  }
  return $label;
}

/**
 * Calculate the applicable discount for a given order.
 */
function uc_order_subs_calculate_discount($order_id) {
  $percentage = intval(variable_get('uc_order_subs_discount', 0));
  $order = uc_order_load($order_id);
  $cart = uc_cart_get_contents();
  $subtotal = 0;

  // Discount is being calculated during checkout so calculate it on the items
  // in the cart.
  if (!empty($cart)) {
    $context = array(
      'revision' => 'altered',
      'type' => 'cart_item',
    );
    foreach ($cart as $item) {
      $price_info = array(
        'price' => $item->price,
        'qty' => ($item->qty) ? $item->qty : 1,
      );
      $context['subject'] = array(
        'cart_item' => $item,
        'node' => node_load($item->nid),
      );
      $total = uc_price($price_info, $context);
      $subtotal += $total;
    }
  }

  // Discount is being calculated after-the-fact so calculate it on the
  // $order->products array.
  else {
    foreach ($order->products as $item) {
      $node = node_load($item->nid);
      $context = array(
        'revision' => 'altered',
        'type' => 'cart_item',
        'subject' => array(
          'cart_item' => $item,
          'node' => $node,
        ),
      );
      $price_info = array(
        'price' => $item->price,
        'qty' => $item->qty,
      );

      $subtotal += uc_price($price_info, $context);
    }
  }

  return -($subtotal * $percentage * 0.01);
}

/**
 * Build a list of active subscription records that need to be processed.
 *
 * @return
 *   Array of values.
 */
function uc_order_subs_load_active() {
  $subscriptions = array();

  $date_number = variable_get('uc_order_subs_date_number', 1);
  $date_type = variable_get('uc_order_subs_date_type', 'month');
  $datestr = 'midnight -' . $date_number . $date_type . ' -1day';
  $date = strtotime($datestr);

  $query = db_query('SELECT r.sub_id, r.order_id, r.active, r.uid, r.created, r.updated, from_unixtime(r.created) AS created_datetime FROM {uc_order_subs} r WHERE r.active = 1');

  while ($result = db_fetch_object($query)) {
    $subscription = db_fetch_object(db_query('SELECT o.sublog_id, o.created, from_unixtime(o.created) AS created_datetime FROM {uc_order_subs_log} o WHERE o.sub_id = %d ORDER BY o.created DESC LIMIT 1', array($result->sub_id)));
    if (empty($subscription) || intval($subscription->created) < $date) {
      $subscriptions[] = $result;
    }
  }

  return $subscriptions;
}

/**
 * Clone an order.
 *
 * @param $sub_id
 *   The ID of the record.
 */
function uc_order_subs_copy_order($sub_id) {
  // Ensure there's an order_id to work from.
  $base_order_id = db_result(db_query('SELECT order_id FROM {uc_order_subs} WHERE sub_id = %d AND active = %d', array($sub_id, 1)));
  if (empty($base_order_id)) {
    watchdog('uc_order_subs', 'Unable to load the order ID created for record @sub_id.', array('@sub_id' => $sub_id), WATCHDOG_INFO);
    return;
  }

  // Ensure there's a valid base order to work from.
  $base_order = uc_order_load($base_order_id);
  if (empty($base_order)) {
    watchdog('uc_order_subs', 'Unable to load the base order @base_order_id.', array('@base_order_id' => $base_order_id), WATCHDOG_INFO);
    return;
  }

  // Clone the basic order data from the old one.
  $new_order = new UcOrder();
  $new_order->uid = $base_order->uid;
  $new_order->order_status = uc_order_state_default('post_checkout');
  $new_order->created = time();
  $new_order->modified = time();

  $values = array('primary_email', 'delivery_first_name', 'delivery_last_name', 'delivery_phone', 'delivery_company', 'delivery_street1', 'delivery_street2', 'delivery_city', 'delivery_zone', 'delivery_postal_code', 'delivery_country', 'billing_first_name', 'billing_last_name', 'billing_phone', 'billing_company', 'billing_street1', 'billing_street2', 'billing_city', 'billing_zone', 'billing_postal_code', 'billing_country', 'quote');
  foreach ($values as $value) {
    $new_order->$value = $base_order->$value;
  }
  drupal_write_record('uc_orders', $new_order);
  uc_order_module_invoke('new', $new_order, NULL);

  // Copy the products.
  foreach ($base_order->products as $key => $base_product) {
    $new_product = (array) $base_product;
    $new_product = (object) $new_product;
    unset($new_product->order_product_id);
    $new_product->order_id = $new_order->order_id;
    $new_order->products[] = $new_product;
  }

  // Copy the shipping details.
  foreach ($base_order->line_items as $key => $line_item) {
    if ($line_item['type'] == 'shipping') {
      uc_order_line_item_add($new_order->order_id,
        'shipping',
        $line_item['title'],
        $line_item['amount'],
        $line_item['weight'],
        $line_item['data']
      );
    }
  }

  // Save the order.
  uc_order_save($new_order);

  // Add a line item for the discount if one was not added already. This is
  // mainly for backwards compatibility and will be removed later.
  // TODO: remove this at some point.
  uc_order_subs_add_discount($order_id);

  // Log the subscribed order.
  $order_log = new StdClass();
  $order_log->sub_id = $sub_id;
  $order_log->order_id = $new_order->order_id;
  $order_log->created = time();
  $order_log->comment = "Created during cron at " . date('r');
  drupal_write_record('uc_order_subs_log', $order_log);

  watchdog('uc_order_subs', 'Order @order_id created for user @uid from base order @base_order_id.', array('@order_id' => $new_order->order_id, '@uid' => $new_order->uid, '@base_order_id' => $base_order_id), WATCHDOG_INFO);
}
