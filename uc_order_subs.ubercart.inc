<?php
/**
 * @file
 * Implementations of Ubercart hooks.
 */

/**
 * Implementation of hook_checkout_pane().
 */ 
function uc_order_subs_checkout_pane() {
  $panes = array();

  $panes[] = array(
    'id' => 'uc_order_subs',
    'callback' => '_uc_order_subs_pane_callback',
    'title' => variable_get('uc_order_subs_title', 'Create subscription order'),
    'desc' => t('Lets users schedule an order to be sent regularly.'),
    'weight' => 0,
    'enabled' => FALSE,
    'process' => TRUE,
    'collapsible' => FALSE,
  );  

  return $panes;
} 

/**
 * Implementation of hook_uc_checkout_complete().
 */
function uc_order_subs_uc_checkout_complete($order, $account) {
  if (!empty($_SESSION['uc_order_subs'])) {
    $obj = new StdClass();
    $obj->uid = $order->uid;
    $obj->order_id = $order->order_id;
    $obj->created = time();
    $obj->updated = $obj->created;
    $obj->active = 1;

    drupal_write_record('uc_order_subs', $obj);
    if (!empty($obj->sub_id)) {
      drupal_set_message(t('The order has been scheduled.'));
      watchdog('uc_order_subs', 'Order @order_id scheduled for user @uid', array('@order_id' => $order->order_id, '@uid' => $order->uid), WATCHDOG_INFO);
    }
    else {
      watchdog('uc_order_subs', 'Problem scheduling order @order_id for user @uid', array('@order_id' => $order->order_id, '@uid' => $order->uid), WATCHDOG_ERROR);
    }
  }
}

/**
 * Callbacks.
 */

/**
 * UC checkout pane callback.
 *
 * @param $op
 * @param $arg1
 *   The current order object if it exists.
 * @param $arg2
 *   The contents of the array of the submitted form for that pane. 
 */
function _uc_order_subs_pane_callback($op, $arg1, $arg2) {
  switch ($op) {
    // Display the form.
    case 'view':
      return array(
        'contents' => array(_uc_order_subs_pane_form($arg1->order_id)),
        'description' => variable_get('uc_order_subs_desc', 'A subscription may be created from this order.'),
      );
      break;

    // Save.
    case 'process':
      _uc_order_subs_pane_flag($arg1->order_id, $arg2);
      return TRUE;
      break;

    // Display the settings.
    case 'review':
      return _uc_order_subs_pane_review($arg1->order_id);
      break;

    // Erm. Settings.
    case 'settings':
      $form = array();
      $form['uc_order_subs_title'] = array(
        '#type' => 'textfield',
        '#title' => t("Title for checkout pane"),
        '#default_value' => variable_get('uc_order_subs_title', 'Create subscription'),
        '#required' => TRUE,
      );
      $form['uc_order_subs_desc'] = array(
        '#type' => 'textarea',
        '#title' => t("Intro message checkout pane"),
        '#default_value' => variable_get('uc_order_subs_desc', 'A subscription may be created from this order.'),
        '#required' => FALSE,
      );
      $form['uc_order_subs_discount'] = array(
        '#type' => 'textfield',
        '#title' => t("Percentage discount for subscriptions"),
        '#default_value' => variable_get('uc_order_subs_discount', 0),
        '#description' => t('Can be any number, whole (1, 8, etc) or decimal number (1.5, 3.75, etc). Set to 0 to disable.'),
        '#size' => 5,
        '#maxlength' => 10,
        '#required' => TRUE,
      );
      $form['uc_order_subs'] = array(
        '#type' => 'fieldset',
        '#title' => t('Duration settings'),
        '#description' => t('Combine these options to control how often orders are sent, e.g. 2 weeks, 1 month, 60 days, etc.'),
        '#collapsible' => FALSE,
      );
      $form['uc_order_subs']['uc_order_subs_date_number'] = array(
        '#type' => 'select',
        '#title' => t("Duration length"),
        '#default_value' => variable_get('uc_order_subs_date_number', 1),
        '#options' => drupal_map_assoc(range(1, 60)),
        '#required' => TRUE,
      );
      $form['uc_order_subs']['uc_order_subs_date_type'] = array(
        '#type' => 'select',
        '#title' => t("Duration type"),
        '#default_value' => variable_get('uc_order_subs_date_type', 'month'),
        '#options' => array('day' => t("Days"), 'week' => t('Weeks'), 'month' => t('Months')),
        '#required' => TRUE,
      );
      
      return $form;
      break;

    default:
      break;
  }
}

/**
 * FormAPI callback for the checkoutbox.
 */
function _uc_order_subs_pane_form($order_id) {
  $form = array();

  $date_number = variable_get('uc_order_subs_date_number', 1);
  $date_type = variable_get('uc_order_subs_date_type', 'month');
  $form['subscription'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send this to me every <strong>!count</strong>.', array('!count' => format_plural($date_number, "1 {$date_type}", "@count {$date_type}s"))),
    '#default_value' => !empty($_SESSION['uc_order_subs']) ? TRUE : FALSE,
  );

  return $form;
}

/**
 * Callback for informing the visitor.
 */
function _uc_order_subs_pane_review($order_id) {
  if (!empty($_SESSION['uc_order_subs'])) {
    $date_number = variable_get('uc_order_subs_date_number', 1);
    $date_type = variable_get('uc_order_subs_date_type', 'month');
    return t('Order will be sent ever <strong>!count</strong>.', array('!count' => format_plural($date_number, "1 {$date_type}", "@count {$date_type}s")));
  }
}

/**
 * Callback to save the checkout settings.
 */
function _uc_order_subs_pane_flag($order_id, $arg2) {
  if (!empty($arg2[0]['subscription'])) {
    // Add the discount to this order.
    uc_order_subs_add_discount($order_id);

    $_SESSION['uc_order_subs'] = $order_id;
  }
  else {
    // Remove any subscription discounts that might have been added previously.
    uc_order_subs_remove_discount($order_id);

    $_SESSION['uc_order_subs'] = FALSE;
  }
}
