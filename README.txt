Ubercart Order Subscriptions
----------------------------
Provides a way for visitors to select their current order to be sent on a
subscription basis. The store administrator can control the schedule on which
the orders are sent, allowing for both the number and type (day, week, month)
of the duration to be controlled, along with an optional discount.

Installation
------------
* Install and enable the module as normal.
* Visit the Checkout Panes admin page and decide where on the checkout page
  the "Create order subscription" pane should show:
    admin/store/settings/checkout/edit/panes
* Change the title and intro message as necessary.
* Select the duration settings that new orders will follow. Examples include:
  * To have orders ship every month, select a duration length of "1" and a
    duration type of "month".

Limitations
-----------
* Orders are not automatically invoiced, the store admin will have to manually
  handle the order payment.
* Orders created at the end of the month (29th, 30th, 31st) may have an
  unreliable schedule during months with fewer days than the month in which
  the order was created.

Todo
----
* Provide a mechanism for orders to be automatically invoiced.
* Provide a page for store admins to review current order subscriptions.
* Provide a page for visitors to see their current open subscription(s).

Credits / Contact
-----------------
The module was created and is maintained by Damien McKenna
(http://drupal.org/user/108450).

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  http://drupal.org/project/issues/
